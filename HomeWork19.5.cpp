﻿#include <iostream>

class Animal
{
public:
   virtual ~Animal() {};

    virtual void Voice()
    {
        std::cout << "bla bla bla\n";
    }
};

class Dog : public Animal
{

public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }

};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }

};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

int main()
{
    Animal* beasts[3];
    beasts[0] = new Dog();
    beasts[1] = new Cat();
    beasts[2] = new Cow();

    for (Animal* p : beasts)
    {
        p->Voice();
        delete p;
    }
}

